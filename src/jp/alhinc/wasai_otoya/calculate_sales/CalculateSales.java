package jp.alhinc.wasai_otoya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CalculateSales {

	public static void main(String[] args) {

		try {

			// 支店マップコレクション
			Map<String, String> branchmap = new HashMap<>();
			// 売上マップコレクション
			Map<String, Long> selesmap = new HashMap<>();

			// Fileクラスに読み込むファイルを指定
			File branchfile = new File(args[0], "branch.lst");

			// ファイルが存在しない場合
			if (!branchfile.exists()) {
				System.out.print("支店定義ファイルが存在しません");
				return;
			}

			// 支店ファイル読み込み----------------------------------------------------
			BufferedReader br = null;

			try {
				// 1行ずつ読み込み
				br = new BufferedReader(new FileReader(branchfile));

				String line;
				while ((line = br.readLine()) != null) {
					String[] data = line.split(",", -1);
					if (data.length != 2 || !data[0].matches("^[0-9]{3}$") || data[1].trim().length() < 1) {
						System.out.print("支店定義ファイルのフォーマットが不正です");
						return;
					}

					branchmap.put(data[0], data[1]);
					selesmap.put(data[0], 0l);
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				br.close();
			}

			// ---------------------------------------------------------------------------------

			// 売上ファイル読み込み↓-----------------------------------------------------------

			// フィルタを作成する
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String str) {
					// 条件を指定する
					return new File(dir, str).isFile() && str.matches("^[0-9]{8}.rcd");
				}
			};

			File[] salesFiles = new File(args[0]).listFiles(filter);
			List<String> rcdNumber = Arrays.stream(salesFiles)
					.map(f -> f.getName().substring(0, 8))
					.sorted()
					.collect(Collectors.toList());

			int min = Integer.parseInt(rcdNumber.get(0));
			int max = Integer.parseInt(rcdNumber.get(rcdNumber.size() - 1));
			int num = rcdNumber.size();

			if (max - min + 1 != num) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			for (File salesFile : salesFiles) {
				BufferedReader br2 = null;
				try {

					// 1行ずつ読み込む
					br2 = new BufferedReader(new FileReader(salesFile));

					// 1行目（支店コード）
					String codedata = br2.readLine();

					if (!selesmap.containsKey(codedata)) {
						System.out.println(salesFile + "の支店コードが不正です");
						return;
					}

					// 2行目（売上金額）
					String selldata = br2.readLine();

					// 3行目（不要なデータ）
					String errordata = br2.readLine();
					if (errordata != null) {
						System.out.print(salesFile + "のフォーマットが不正です");
						return;
					}

					long newsell = Long.parseLong(selldata);
					long sum = newsell + selesmap.get(codedata);

					// 売上金額チェック
					String strsum = String.valueOf(Math.abs(sum));
					if (strsum.length() > 10) {
						System.out.print("合計金額が10桁を超えました");
						return;
					}

					selesmap.put(codedata, sum);
				} finally {
					br2.close();
				}

				// 支店別集計ファイル出力-----------------------------------------------------

				File totalfile = new File(args[0], "branch.out");

				BufferedWriter bw = null;
				try {
					bw = new BufferedWriter(new FileWriter(totalfile));

					// ファイルに書き込む

					for (String key : selesmap.keySet()) {
						Long value = selesmap.get(key);
						bw.write(String.format("%s,%s,%s", key, branchmap.get(key), value));
						// 出力ファイルの中身確認
						bw.newLine();
					}

					// ファイルを閉じる
				} finally {
					bw.close();
				}
				// -----------------------------------------------------------------------
			}
		// -------------------------------------------------------------------------------
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}

